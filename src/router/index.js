/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Donations from '@/components/Donations'
import Login from '@/components/Login'
import UserHome from '@/components/userHome'
import BlogAdd from '@/components/BlogAdd'
import UserHome2 from '@/components/userHome2'
import CommentAdd from '@/components/CommentAdd'
import ShowAllUser from '@/components/showAllUser'





Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'register',
      }
    },
    {
      path: '/donations',
      name: 'Donations',
      component: Donations
    },
    {
      path:'/login',
      name:'Login',
      component: Login,
      meta: {
        title: 'Login',
      }

    },
    {
      path:'/userHome',
      name: 'UserHome',
      component:UserHome,
      meta: {
        title: 'UserHome',
      }
    },
    {
      path:'/blogAdd',
      name: 'BlogAdd',
      component:BlogAdd,
      meta: {
        title: 'BlogAdd',
      }
    },
    {
      path:'/userHome2',
      name: 'UserHome2',
      component:UserHome2,
      meta: {
        title: 'UserHome2',
      }
    },
    {
      path:'/commentAdd',
      name: 'CommentAdd',
      component:CommentAdd,
      meta: {
        title: 'CommentAdd',
      }
    },
    {
      path:'/showAllUser',
      name: 'ShowAllUser',
      component:ShowAllUser,
      meta: {
        title: 'ShowAllUser',
      }
    }
  ]
})
