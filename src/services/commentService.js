/* eslint-disable */
import Api from '@/services/api'

export default {
  findCommentByBlogId(blogId){
    return Api().get(`/comment/${blogId}/findByBlogId`)
  },
  addComment(commentInfo,userId,blogId){
    return Api().post(`/comment/${userId}/add/${blogId}`, commentInfo,
      { headers: {'Content-type': 'application/json'} })
  },


}
