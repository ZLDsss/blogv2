/* eslint-disable */
import Api from '@/services/api'

export default {
  findBlogById(userId){
    return Api().get(`/blog/${userId}/findByUserId`)
  },
  addBlog(blogInfo,userId){
    return Api().post(`/blog/${userId}/add`, blogInfo,
      { headers: {'Content-type': 'application/json'} })
  },



}
