/* eslint-disable */
import Api from '@/services/api'

export default {
  findAllUser () {
    return Api().get('/blog/findAll')
  },
  register(userInfo){
    return Api().post('/user/registration', userInfo,
      { headers: {'Content-type': 'application/json'} })
  },
  login(userInfo){
    return Api().post('/user/login', userInfo,
      { headers: {'Content-type': 'application/json'} })
  },
  findUserByName(name){
    return Api().get(`/user/${name}/findByName`)
  },
  findUserByUserId(userId){
    return Api().get(`/user/${userId}/findById`)
  }


}
